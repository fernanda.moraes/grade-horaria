package model;

import java.io.Serializable;

/**
 *
 * @author Fernanda
 */
public class Professor implements Serializable {
    
    private int id;
    private String nome;
    private String apelido;
    private int ch;
    private String email;

    public Professor() {}

    public Professor(int id, String nome, String apelido, int ch, String email) {
        this.id = id;
        this.nome = nome;
        this.apelido = apelido;
        this.ch = ch;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public int getCh() {
        return ch;
    }

    public void setCh(int ch) {
        this.ch = ch;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return this.getNome();
    }
    
    
}
