package model;

import java.io.Serializable;
import model.ProfessorDisciplina;

public class TurmaDisciplinaProfessor extends ProfessorDisciplina implements Serializable {
    
    private Turma turma;
    private GradeCurricular gradeCurricular;
    private int numAulasGeminadas;
    private TipoSala[] tipoSala;
    private Sala[] sala;
    private int[] aulasPorSala;
    
    public TurmaDisciplinaProfessor() {
    }

    /**
     * @return the turma
     */
    public Turma getTurma() {
        return turma;
    }

    /**
     * @param turma the turma to set
     */
    public void setTurma(Turma turma) {
        this.turma = turma;
    }

    /**
     * @return the gradeCurricular
     */
    public GradeCurricular getGradeCurricular() {
        return gradeCurricular;
    }

    /**
     * @param gradeCurricular the gradeCurricular to set
     */
    public void setGradeCurricular(GradeCurricular gradeCurricular) {
        this.gradeCurricular = gradeCurricular;
    }

    /**
     * @return the numAulasGeminadas
     */
    public int getNumAulasGeminadas() {
        return numAulasGeminadas;
    }

    /**
     * @param numAulasGeminadas the numAulasGeminadas to set
     */
    public void setNumAulasGeminadas(int numAulasGeminadas) {
        this.numAulasGeminadas = numAulasGeminadas;
    }

    /**
     * @return the tipoSala
     */
    public TipoSala[] getTipoSala() {
        return tipoSala;
    }

    /**
     * @param tipoSala the tipoSala to set
     */
    public void setTipoSala(TipoSala[] tipoSala) {
        this.tipoSala = tipoSala;
    }

    /**
     * @return the sala
     */
    public Sala[] getSala() {
        return sala;
    }

    /**
     * @param sala the sala to set
     */
    public void setSala(Sala[] sala) {
        this.sala = sala;
    }

    /**
     * @return the aulasPorSala
     */
    public int[] getAulasPorSala() {
        return aulasPorSala;
    }

    /**
     * @param aulasPorSala the aulasPorSala to set
     */
    public void setAulasPorSala(int[] aulasPorSala) {
        this.aulasPorSala = aulasPorSala;
    }

}
