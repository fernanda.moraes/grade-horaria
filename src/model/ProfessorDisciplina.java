package model;

import java.io.Serializable;
import model.Disciplina;

public class ProfessorDisciplina implements Serializable {

    private Professor professor;
    private Disciplina disciplina;
    private int chresponsavel;
    private int chAulasGeminadas;
    
    public ProfessorDisciplina() {
    }

    /**
     * @return the professor
     */
    public Professor getProfessor() {
        return professor;
    }

    /**
     * @param professor the professor to set
     */
    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    /**
     * @return the disciplina
     */
    public Disciplina getDisciplina() {
        return disciplina;
    }

    /**
     * @param disciplina the disciplina to set
     */
    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    /**
     * @return the chresponsavel
     */
    public int getChresponsavel() {
        return chresponsavel;
    }

    /**
     * @param chresponsavel the chresponsavel to set
     */
    public void setChresponsavel(int chresponsavel) {
        this.chresponsavel = chresponsavel;
    }

    /**
     * @return the chAulasGeminadas
     */
    public int getChAulasGeminadas() {
        return chAulasGeminadas;
    }

    /**
     * @param chAulasGeminadas the chAulasGeminadas to set
     */
    public void setChAulasGeminadas(int chAulasGeminadas) {
        this.chAulasGeminadas = chAulasGeminadas;
    }

}
