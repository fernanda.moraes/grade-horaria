package model;

import java.io.Serializable;
import model.Disciplina;

public class DisciplinaTipoSalas implements Serializable {
   
    private int id;
    private Disciplina disciplina;
    private TipoSala tipoSala;
    private int chsem;
    private boolean preferencial;
    
    public DisciplinaTipoSalas() {}

 
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public Disciplina getDisciplina() {
        return disciplina;
    }
    
    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }
    
    public TipoSala getTipoSala() {
        return tipoSala;
    }

    public void setTipoSala(TipoSala tipoSala) {
        this.tipoSala = tipoSala;
    }

    public int getChSemanal() {
        return chsem;
    }

    public void setChSemanal(int chsem) {
        this.chsem = chsem;
    }

    public boolean isPreferencial() {
        return preferencial;
    }
    
    public void setPreferencial(boolean preferencial) {
        this.preferencial = preferencial;
    }

}
