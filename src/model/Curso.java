package model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Fernanda
 */

public class Curso implements Serializable{
    
    private int id;
    private String codigo;
    private String nome;
    private String nomeCurto;
    
    private List<GradeCurricular> gradeCurricular;

    public Curso() {}

    public Curso(String codigo, String nome, String nomeCurtp) {
        this.codigo = codigo;
        this.nome = nome;
        this.nomeCurto = nomeCurtp;
    }

    public String getCodigo() {
        return codigo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<GradeCurricular> getGradeCurricular() {
        return gradeCurricular;
    }

    public void setGradeCurricular(List<GradeCurricular> gradeCurricular) {
        this.gradeCurricular = gradeCurricular;
    }
    
    

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeCurto() {
        return nomeCurto;
    }

    public void setNomeCurto(String nomeCurtp) {
        this.nomeCurto = nomeCurtp;
    }

    @Override
    public String toString() {
        return getNome(); 
    }

}
