package model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Fernanda
 */

public class Turma implements Serializable {
    
    private int id;
    private int codigo;
    private String descricao;
    private Turno turno;
    private int numDias;
    private int numAulas;
    private int periodo;
    private String identificacao;
   
    private Curso curso;
    private GradeItem gradeItem;
    private List<TurmaDisciplinaProfessor> disciplinasProfessores;

    public Turma() {}
    
    public Turma(int codigo, String descricao, Turno turno, int numDias, int numAulas, int periodo, String identificacao, Curso curso) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.turno = turno;
        this.numDias = numDias;
        this.numAulas = numAulas;
        this.periodo = periodo;
        this.identificacao = identificacao;
        this.curso = curso;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public GradeItem getGradeItem() {
        return gradeItem;
    }

    public void setGradeItem(GradeItem gradeItem) {
        this.gradeItem = gradeItem;
    }

    public List<TurmaDisciplinaProfessor> getDisciplinasProfessores() {
        return disciplinasProfessores;
    }

    public void setDisciplinasProfessores(List<TurmaDisciplinaProfessor> disciplinasProfessores) {
        this.disciplinasProfessores = disciplinasProfessores;
    }

    
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Turno getTurno() {
        return turno;
    }

    public void setTurno(Turno turno) {
        this.turno = turno;
    }

    public int getNumDias() {
        return numDias;
    }

    public void setNumDias(int numDias) {
        this.numDias = numDias;
    }

    public int getNumAulas() {
        return numAulas;
    }

    public void setNumAulas(int numAulas) {
        this.numAulas = numAulas;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public String getIdentificacao() {
        return identificacao;
    }

    public void setIdentificacao(String identificacao) {
        this.identificacao = identificacao;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

}
