package model;

import java.io.Serializable;

/**
 *
 * @author Fernanda
 */

public class TipoSala implements Serializable {
    
    private int id;
    private int tipo;
    private String descricao;
    private boolean padrao;
    private int capacidade;

    public TipoSala() {}

    public TipoSala(int tipo, String descricao, boolean padrao) {
        this.tipo = tipo;
        this.descricao = descricao;
        this.padrao = padrao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    
    
    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean isPadrao() {
        return padrao;
    }

    public void setPadrao(boolean padrao) {
        this.padrao = padrao;
    }
}
