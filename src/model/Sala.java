package model;

import java.io.Serializable;
import java.util.List;

public class Sala implements Serializable {

    private int id;
    private String descricao;
    private TipoSala tipo;
    private int capacidade;
    private List<PreferenciaSala> preferenciaSala;
    
    public Sala() {
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the tipo
     */
    public TipoSala getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(TipoSala tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the capacidade
     */
    public int getCapacidade() {
        return capacidade;
    }

    /**
     * @param capacidade the capacidade to set
     */
    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    /**
     * @return the preferenciaSala
     */
    public List<PreferenciaSala> getPreferenciaSala() {
        return preferenciaSala;
    }

    /**
     * @param preferenciaSala the preferenciaSala to set
     */
    public void setPreferenciaSala(List<PreferenciaSala> preferenciaSala) {
        this.preferenciaSala = preferenciaSala;
    }

}
