package model;

import java.io.Serializable;

public class GradeItem implements Serializable {
    
    private int id;
    private GradeCurricular gradeCurricular;
    private int periodo;
    private Disciplina disciplina;
    private int chtotal;
    private int chSemanal;
    private int chTeorica;
    private int chPratica;
    private int chEad;
    
    public GradeItem() {
    }
    
    public GradeItem(GradeCurricular grade) {
        this.gradeCurricular = grade;
    }

    public GradeCurricular getGradeCurricular() {
        return gradeCurricular;
    }

    public void setGradeCurricular(GradeCurricular gradeCurricular) {
        this.gradeCurricular = gradeCurricular;
    }

    public int getPeriodo() {
        return periodo;
    }
    
    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    public int getChtotal() {
        return chtotal;
    }
    
    public void setChtotal(int chtotal) {
        this.chtotal = chtotal;
    }

    public int getChSemanal() {
        return chSemanal;
    }

    public void setChSemanal(int chSemanal) {
        this.chSemanal = chSemanal;
    }

    public int getChTeorica() {
        return chTeorica;
    }

    public void setChTeorica(int chTeorica) {
        this.chTeorica = chTeorica;
    }

    public int getChPratica() {
        return chPratica;
    }

    public void setChPratica(int chPratica) {
        this.chPratica = chPratica;
    }

    public int getChEad() {
        return chEad;
    }

    public void setChEad(int chEad) {
        this.chEad = chEad;
    }
    
}
