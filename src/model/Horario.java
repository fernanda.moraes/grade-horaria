package model;

import java.io.Serializable;

public class Horario implements Serializable {

    private int id;
    private int codigo;
    private Turno turno;
    private String descricao;
    private int ordem;

    public Horario() {
    }
    
    public Horario(int cod) {
        this.codigo = cod;
    }
    
    public Horario(int cod, Turno tur) {
        this.codigo = cod;
        this.turno = tur;
    }
    
    public Horario(int cod, Turno tur, String desc) {
        this.codigo = cod;
        this.turno = tur;
        this.descricao = desc;
    }
    
    public Horario(int cod, Turno tur, String desc, int ordem) {
        this.codigo = cod;
        this.turno = tur;
        this.descricao = desc;
        this.ordem = ordem;
    }
    
    public Horario(int cod, String desc, int ordem) {
        this.codigo = cod;
        this.turno = null;
        this.descricao = desc;
        this.ordem = ordem;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the turno
     */
    public Turno getTurno() {
        return turno;
    }

    /**
     * @param turno the turno to set
     */
    public void setTurno(Turno turno) {
        this.turno = turno;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the ordem
     */
    public int getOrdem() {
        return ordem;
    }

    /**
     * @param ordem the ordem to set
     */
    public void setOrdem(int ordem) {
        this.ordem = ordem;
    }

}
