
package model;

import java.io.Serializable;

/**
 *
 * @author Fernanda
 */
public class Preferencia implements Serializable {
    
    private int id;
    private Horario horario;
    private int preferencia;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Horario getHorario() {
        return horario;
    }

    public void setHorario(Horario horario) {
        this.horario = horario;
    }

    
}
