package model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Fernanda
 */

public class Turno implements Serializable {
    
    private int id;
    private String turno;
    private String descricao;
    private int numAulas;
    private int intervalo;
    private List<Horario> horarios;

    public Turno() { }

    public Turno(String turno, String descricao, int numAulas, int intervalo) {
        this.turno = turno;
        this.descricao = descricao;
        this.numAulas = numAulas;
        this.intervalo = intervalo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Horario> getHorarios() {
        return horarios;
    }

    public void setHorarios(List<Horario> horarios) {
        this.horarios = horarios;
    }
    
    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getNumAulas() {
        return numAulas;
    }

    public void setNumAulas(int numAulas) {
        this.numAulas = numAulas;
    }

    public int getIntervalo() {
        return intervalo;
    }

    public void setIntervalo(int intervalo) {
        this.intervalo = intervalo;
    }

    @Override
    public String toString() {
        return turno;
    }
}
