package model;

import java.util.Date;
import java.io.Serializable;
import java.util.List;

public class Grade implements Serializable {

    private int id;
    private Turno turno;
    private List<GradeHistorico> gradeHistorico;

    private Date dataInicio;
    private Date dataFim;
    private Date dataCriacao;
    private Date dataAtualizacao;

    private List<Aula> aulas;
    
    public Grade() {
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public Turno getTurno() {
        return turno;
    }

    public void setTurno(Turno turno) {
        this.turno = turno;
    }

    public List<GradeHistorico> getGradeHistorico() {
        return gradeHistorico;
    }

    public void setGradeHistorico(List<GradeHistorico> gradeHistorico) {
        this.gradeHistorico = gradeHistorico;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }
    
    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public Date getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(Date dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }
    
    public List<Aula> getAulas() {
        return aulas;
    }
    
    public void setAulas(List<Aula> aulas) {
        this.aulas = aulas;
    }

}
