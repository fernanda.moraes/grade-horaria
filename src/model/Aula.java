package model;

import java.io.Serializable;

/**
 *
 * @author Fernanda
 */

public class Aula implements Serializable{
    
    private int id;
    private Turma turma;
    private Sala sala;
    private Disciplina disciplina;
    private Professor professor;
    private Grade grade;

    public Aula() {}

    public Aula(Turma turma, Sala sala, Disciplina disciplina, Professor professor, Grade grade) {
        this.turma = turma;
        this.sala = sala;
        this.disciplina = disciplina;
        this.professor = professor;
        this.grade = grade;
    }

    public Turma getTurma() {
        return turma;
    }

    public void setTurma(Turma turma) {
        this.turma = turma;
    }

    public Sala getSala() {
        return sala;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public Grade getGrade() {
        return grade;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }
    
}
