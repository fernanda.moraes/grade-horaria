
package model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Fernanda
 */
public class Disciplina implements Serializable{
    
    private int id;
    private String codigo;
    private String nome;
    private int chSem;
    private int chAulasGeminadas;
    
    private List<DisciplinaTipoSalas> disciplinaTipoSalas;
        
    public Disciplina() {}

    public Disciplina(int id, String codigo, String nome, int chSem, int chAulasGeminadas) {
        this.id = id;
        this.codigo = codigo;
        this.nome = nome;
        this.chSem = chSem;
        this.chAulasGeminadas = chAulasGeminadas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getChSem() {
        return chSem;
    }

    public void setChSem(int chSem) {
        this.chSem = chSem;
    }

    public int getChAulasGeminadas() {
        return chAulasGeminadas;
    }

    public void setChAulasGeminadas(int chAulasGeminadas) {
        this.chAulasGeminadas = chAulasGeminadas;
    }
    
}
