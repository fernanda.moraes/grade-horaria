package model;

import java.util.Date;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GradeCurricular implements Serializable {

    private int id;
    private Curso curso;
    private String gradeVersao;
    private Date dataImplantacao;
    private Date dataEncerramento;
    private List<GradeItem> itens;
    
    public GradeCurricular() {
        itens = new ArrayList<GradeItem>();
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public String getGradeVersao() {
        return gradeVersao;
    }

    public void setGradeVersao(String gradeVersao) {
        this.gradeVersao = gradeVersao;
    }

    public Date getDataImplantacao() {
        return dataImplantacao;
    }

    public void setDataImplantacao(Date dataImplantacao) {
        this.dataImplantacao = dataImplantacao;
    }
    
    public Date getDataEncerramento() {
        return dataEncerramento;
    }

    public void setDataEncerramento(Date dataEncerramento) {
        this.dataEncerramento = dataEncerramento;
    }

    public List<GradeItem> getItens() {
        return itens;
    }

    public void setItens(List<GradeItem> itens) {
        this.itens = itens;
    }
    

}
