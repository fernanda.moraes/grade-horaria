package model;

import java.util.Date;
import java.io.Serializable;

public class GradeHistorico implements Serializable {
    
    private int id;
    private Grade grade;
    private Date datahora;
    private String usuario;
    private String comentario;
    
    public GradeHistorico() {
    }
    
    public GradeHistorico(Grade grade) {
        this.grade = grade;
    }

    public Grade getGrade() {
        return grade;
    }

  
    public void setGrade(Grade grade) {
        this.grade = grade;
    }

 
    public Date getDatahora() {
        return datahora;
    }

    public void setDatahora(Date datahora) {
        this.datahora = datahora;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
    

}
