
package adapter;

import java.util.HashMap;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import model.Professor;

/**
 *
 * @author Fernanda
 */
public class ProfessorComboBoxModel extends AbstractListModel<Professor> implements ComboBoxModel<Professor>{

    private HashMap<Integer, Professor> professores;
    private Professor professorSelecionado;

    public ProfessorComboBoxModel(HashMap<Integer, Professor> professores) {
        this.professores = professores;
    }
    
    @Override
    public int getSize() {
        return professores.size();
    }

    @Override
    public Professor getElementAt(int index) {
        return professores.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        professorSelecionado = (Professor) anItem;
    }

    @Override
    public Object getSelectedItem() {
        return  professorSelecionado;
    }

}
