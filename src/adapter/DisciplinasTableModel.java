package adapter;

import java.util.HashMap;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.Disciplina;

/**
 *
 * @author Fernanda
 */
public class DisciplinasTableModel extends AbstractTableModel{

    private HashMap<Integer, Disciplina> disciplinas;

    public DisciplinasTableModel(HashMap<Integer, Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public void setDisciplinas(HashMap<Integer, Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
        fireTableDataChanged();
    }
    
    @Override
    public int getRowCount() {
        return disciplinas.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int column) {
        switch(column) {
            case 0:
                return "ID";
            case 1:
                return "Código";
            case 2:
                return "Nome";
            case 3:
                return "CH Semanal";
            case 4:
                return "Aulas Geminatórias";
            default:
                break;
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
            case 0:
                return int.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return int.class;
            case 4:
                return int.class;
            default:
                break;
        }
        return null;
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Disciplina disciplina = disciplinas.get(rowIndex);
        switch(columnIndex) {
            case 0:
                return disciplina.getId();
            case 1:
                return disciplina.getCodigo();
            case 2:
                return disciplina.getNome();
            case 3:
                return disciplina.getChSem();
            case 4:
                return disciplina.getChAulasGeminadas();
            default:
                break;
        }
        return null;
    }
    
}
