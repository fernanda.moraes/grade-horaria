
package adapter;

import java.util.HashMap;
import javax.swing.table.AbstractTableModel;
import model.TipoSala;

/**
 *
 * @author Fernanda
 */
public class TipoSalaTableModel extends AbstractTableModel{

    private HashMap<Integer, TipoSala> tipoSalas;
    
    public TipoSalaTableModel(HashMap<Integer, TipoSala> tipoSalas) {
        this.tipoSalas = tipoSalas;
    }

    public void setTipoSalas(HashMap<Integer, TipoSala> tipoSalas) {
        this.tipoSalas = tipoSalas;
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return tipoSalas.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        TipoSala tipoSala = tipoSalas.get(rowIndex);
        
        switch(columnIndex){
            case 0:
                return tipoSala.getId();
            case 1:
                return tipoSala.getTipo();
            case 2:
                return tipoSala.getDescricao();
            case 3:
                return tipoSala.isPadrao();
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
         switch(columnIndex){
            case 0:
                return int.class;
            case 1:
                return int.class;
            case 2:
                return String.class;
            case 3:
                return Boolean.class;
        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "ID";
            case 1:
                return "Tipo";
            case 2:
                return "Descrição";
            case 3:
                return "Padrão";
        }
        return null;
    }
    
    
    
    
    
}
