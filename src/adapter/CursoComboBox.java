
package adapter;

import java.util.HashMap;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import model.Curso;

/**
 *
 * @author Fernanda
 */
public class CursoComboBox extends AbstractListModel<Curso> implements ComboBoxModel<Curso>{

    private HashMap<Integer, Curso> cursos;
    private Curso cursoSelecionado;
    
    public CursoComboBox(HashMap<Integer, Curso> cursos) {
        this.cursos = cursos;
    }
    
    @Override
    public int getSize() {
        return cursos.size();
    }

    @Override
    public Curso getElementAt(int index) {
        return cursos.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        cursoSelecionado = (Curso) anItem;
    }

    @Override
    public Object getSelectedItem() {
        return cursoSelecionado;
    }
    
}
