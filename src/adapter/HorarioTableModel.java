
package adapter;

import java.util.HashMap;
import javax.swing.table.AbstractTableModel;
import model.Horario;

/**
 *
 * @author Fernanda
 */
public class HorarioTableModel extends AbstractTableModel{
    
    private HashMap<Integer,Horario> horarios;

    public HorarioTableModel(HashMap<Integer,Horario> horarios) {
        this.horarios = horarios;
    }

    @Override
    public int getRowCount() {
        return horarios.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    public void setHorarios(HashMap<Integer,Horario> horarios) {
        this.horarios = horarios;
        fireTableDataChanged();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Horario horario = horarios.get(rowIndex);
       
        switch(rowIndex){
            case 0:
                return horario.getId();
            case 1:
                return horario.getCodigo();
            case 2:
                return horario.getDescricao();
            case 3:
                return horario.getTurno().getDescricao();
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex){
            case 0:
                return int.class;
            case 1:
                return int.class;
            case 2:
                return String.class;
            case 3:
                return String.class;
        }
        return null;    
    }

    @Override
    public String getColumnName(int column) {
    switch(column){
            case 0:
                return "ID";
            case 1:
                return "Código";
            case 2:
                return "Descrição";
            case 3:
                return "Turno";
        }
        return null;    
    }
    
}
