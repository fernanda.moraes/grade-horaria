package adapter;

import java.util.HashMap;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import model.Turno;

/**
 *
 * @author Fernanda
 */
public class TurnoComboBox extends AbstractListModel<Turno> implements ComboBoxModel<Turno> {

    private HashMap<Integer, Turno> turnos;
    private Turno turnoSelecionado;

    public TurnoComboBox(HashMap<Integer, Turno> turnos) {
        this.turnos = turnos;
    }
    
    @Override
    public int getSize() {
        return turnos.size();
    }

    @Override
    public Turno getElementAt(int index) {
        return turnos.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        turnoSelecionado = (Turno) anItem;
    }

    @Override
    public Object getSelectedItem() {
        return (Turno) turnoSelecionado;
    }
    
}
