package adapter;

import java.util.HashMap;
import javax.swing.table.AbstractTableModel;
import model.Curso;
import model.Turma;
import model.Turno;

/**
 *
 * @author Fernanda
 */
public class TurmasTableModel extends AbstractTableModel {

    private HashMap<Integer, Turma> turmas;
    
    public TurmasTableModel(HashMap<Integer, Turma> turmas) {
        this.turmas = turmas;
    }
    
    public void setTurmas(HashMap<Integer, Turma> turmas) {
        this.turmas = turmas;
        fireTableDataChanged();
    }
   
    @Override
    public int getRowCount() {
        return turmas.size();
    }

    @Override
    public int getColumnCount() {
       return 5;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
         switch(columnIndex){
            case 0:
                return Integer.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return Turno.class;
            case 4:
                return Curso.class;
            default:
                break;
        }
        return null;
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "ID";
            case 1:
                return "Código";
            case 2:
                return "Período";
            case 3:
                return "Turno";
            case 4:
                return "Curso";
            default:
                break;
        }
        return null;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Turma turma = turmas.get(rowIndex);
        switch(columnIndex){
            case 0:
                return turma.getId();
            case 1:
                return turma.getCodigo();
            case 2:
                return turma.getPeriodo();
            case 3:
                return turma.getTurno();
            case 4:
                return turma.getCurso();
            default:
                break;
        }
        return null;
    }
    
}
