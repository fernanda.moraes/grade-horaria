package adapter;

import java.util.HashMap;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.Turno;

/**
 *
 * @author Fernanda
 */
public class TurnosTableModel extends AbstractTableModel {

    private HashMap<Integer, Turno> turnos;

    public TurnosTableModel(HashMap<Integer, Turno> turnos) {
        this.turnos = turnos;
    }
    
    @Override
    public int getRowCount() {
        return turnos.size();
    }

      @Override
    public String getColumnName(int column){
          switch (column) {
            case 0:
                return "ID";
            case 1:
                return "Turno";
            case 2:
                return "Descrição";
            case 3:
                return "Número de Aulas"; 
            case 4:
                return "Intervalo"; 
            default:
                break;
        }
        return null;
    }
    
    @Override
    public Class getColumnClass(int columnIndex){
        switch (columnIndex) {
            case 0:
                return int.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return Integer.class;
            case 4:
                return Integer.class;
            default:
                break;
        }
        return null;
    }

    public void setCursos(HashMap<Integer, Turno> turnos) {
        this.turnos = turnos;
        fireTableDataChanged();
    }
   
    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
       Turno turno = turnos.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return turno.getId();
            case 1:
                return turno.getTurno();
            case 2:
                return turno.getDescricao();
            case 3:
                return turno.getNumAulas();
            case 4:
                return turno.getIntervalo();
            default:
                break;
        }
        return null;
    }
    
    
    
}
