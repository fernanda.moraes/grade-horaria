
package adapter;

import java.util.HashMap;
import javax.swing.table.AbstractTableModel;
import model.Professor;

/**
 *
 * @author Fernanda
 */
public class ProfessorTableModel extends AbstractTableModel{

    private HashMap<Integer, Professor> professores;

    /**
     *
     * @param professores
     */
    public ProfessorTableModel(HashMap<Integer, Professor>professores) {
        this.professores = professores;
    }

    public void setProfessores(HashMap<Integer, Professor> professores) {
        this.professores = professores;
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return professores.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Professor professor = professores.get(rowIndex);
        switch(columnIndex){
            case 0:
                return professor.getId();
            case 1:
                return professor.getNome();
            case 2: 
                return professor.getApelido();
            case 3:
                return professor.getEmail();
        }
       
        return null;
    }

    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "ID";
            case 1:
                return "Nome";
            case 2: 
                return "Apelido";
            case 3:
                return "Email";
        }
        
        return null;
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex){
            case 0:
                return int.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return String.class;
        }
        
        return null;
    }

    @Override
    public int getColumnCount() {
        return 4;
    }
}
