package adapter;

import java.util.HashMap;
import javax.swing.table.AbstractTableModel;
import model.Curso;

/**
 *
 * @author Fernanda
 */
public class CursosTableModel extends AbstractTableModel{

    private HashMap<Integer, Curso> cursos;

    public CursosTableModel(HashMap<Integer, Curso> cursos) {
        this.cursos = cursos;
    }
    
    @Override
    public int getRowCount() {
        return cursos.size();
    }

      @Override
    public String getColumnName(int column){
          switch (column) {
            case 0:
                return "ID"; 
            case 1:
                return "codigo";
            case 2:
                return "nome";
            case 3:
                return "nomeCurto"; 
            default:
                break;
        }
        return null;
    }
    
    @Override
    public Class getColumnClass(int columnIndex){
        switch (columnIndex) {
            case 0:
                return Integer.class; 
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return String.class;
            default:
                break;
        }
        return null;
    }

    public void setCursos(HashMap<Integer, Curso> cursos) {
        this.cursos = cursos;
        fireTableDataChanged();
    }
   
    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
       Curso curso = cursos.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return curso.getId();
            case 1:
                return curso.getCodigo();
            case 2:
                return curso.getNome();
            case 3:
                return curso.getNomeCurto();
            default:
                break;
        }
        return null;
    }
}
