package view;

import adapter.TurnosTableModel;
import static gradehoraria.GradeHoraria.turnoDAO;
import java.awt.Point;
import java.util.HashMap;
import java.util.List;
import javax.swing.JTable;
import model.Turno;

/**
 *
 * @author Fernanda
 */
public class TurnoJDialog extends javax.swing.JDialog {
    
    private TurnosTableModel turnosTableModel;
    
    public TurnoJDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        turnosTableModel = new TurnosTableModel(turnoDAO.listar());
        initComponents();
        atualizarTabelaTurnos();
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        turnoLabel = new javax.swing.JLabel();
        turnoTextField = new javax.swing.JTextField();
        descricaoLabel = new javax.swing.JLabel();
        descricaoTextField = new javax.swing.JTextField();
        numAulasLabel = new javax.swing.JLabel();
        numAulasTextField = new javax.swing.JTextField();
        intervaloLabel = new javax.swing.JLabel();
        intervaloTextField = new javax.swing.JTextField();
        addTurnoButton = new javax.swing.JButton();
        editarTurnoButton = new javax.swing.JButton();
        excluirTurnoButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        turnoTable = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Turno");

        turnoLabel.setText("Turno:");

        descricaoLabel.setText("Descrição:");

        numAulasLabel.setText("Número de aulas:");

        intervaloLabel.setText("Intervalo:");

        addTurnoButton.setText("Adicionar");
        addTurnoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addTurnoButtonActionPerformed(evt);
            }
        });

        editarTurnoButton.setText("Editar");

        excluirTurnoButton.setText("Excluir");
        excluirTurnoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                excluirTurnoButtonActionPerformed(evt);
            }
        });

        turnoTable.setModel(turnosTableModel);
        turnoTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                turnoTableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(turnoTable);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane2)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(descricaoLabel)
                                    .addComponent(turnoLabel))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(turnoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(descricaoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(numAulasLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(intervaloTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(numAulasTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(455, 492, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(intervaloLabel)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(addTurnoButton)
                                .addGap(18, 18, 18)
                                .addComponent(editarTurnoButton)
                                .addGap(31, 31, 31)
                                .addComponent(excluirTurnoButton)))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(turnoLabel)
                    .addComponent(turnoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(descricaoLabel)
                    .addComponent(descricaoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(numAulasLabel)
                    .addComponent(numAulasTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(intervaloLabel)
                    .addComponent(intervaloTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addTurnoButton)
                    .addComponent(editarTurnoButton)
                    .addComponent(excluirTurnoButton))
                .addGap(37, 37, 37)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(22, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void addTurnoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addTurnoButtonActionPerformed
        Turno turno = getTurno(null, 0);
        turnoDAO.salvar(turno);
        atualizarTabelaTurnos();
        limparCampos();
    }//GEN-LAST:event_addTurnoButtonActionPerformed

    private void excluirTurnoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_excluirTurnoButtonActionPerformed
        Turno turno = getTurno(null, 0);
        turnoDAO.remover(turno);
        atualizarTabelaTurnos();
        limparCampos();
    }//GEN-LAST:event_excluirTurnoButtonActionPerformed

    private void turnoTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_turnoTableMouseClicked
        JTable tabela = (JTable) evt.getSource();
        Point clique = evt.getPoint();
        int linhaClicada = tabela.rowAtPoint(clique);
        
        if(linhaClicada != -1 && evt.getClickCount() == 2) {
            Turno turnoSelecionado = getTurno(tabela, linhaClicada);
            preencheCampos(turnoSelecionado);
        }
    }//GEN-LAST:event_turnoTableMouseClicked

  
    public static void main(String args[]) {
     
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                TurnoJDialog dialog = new TurnoJDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addTurnoButton;
    private javax.swing.JLabel descricaoLabel;
    private javax.swing.JTextField descricaoTextField;
    private javax.swing.JButton editarTurnoButton;
    private javax.swing.JButton excluirTurnoButton;
    private javax.swing.JLabel intervaloLabel;
    private javax.swing.JTextField intervaloTextField;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel numAulasLabel;
    private javax.swing.JTextField numAulasTextField;
    private javax.swing.JLabel turnoLabel;
    private javax.swing.JTable turnoTable;
    private javax.swing.JTextField turnoTextField;
    // End of variables declaration//GEN-END:variables

    private Turno getTurno(JTable tabela, int linha) {
        Turno turno = new Turno();
        
        if(tabela != null){
            turno.setTurno((String) tabela.getModel().getValueAt(linha, 0));
            turno.setDescricao((String) tabela.getModel().getValueAt(linha, 1));
            turno.setNumAulas((int) tabela.getModel().getValueAt(linha, 2));
            turno.setIntervalo((int) tabela.getModel().getValueAt(linha, 3));
        } else {
            turno.setDescricao(descricaoTextField.getText());
            turno.setTurno(turnoTextField.getText());
            turno.setNumAulas(Integer.parseInt(numAulasTextField.getText()));
            turno.setIntervalo(Integer.parseInt(intervaloTextField.getText()));
        }
       
        return turno;
    }

    private void atualizarTabelaTurnos() {
        HashMap<Integer, Turno> turnos = turnoDAO.listar();
        turnosTableModel.setCursos(turnos);
        getContentPane().repaint();
    }

    private void limparCampos() {
        descricaoTextField.setText("");
        turnoTextField.setText("");
        numAulasTextField.setText("");
        intervaloTextField.setText("");
    }

    private void preencheCampos(Turno turnoSelecionado) {
        descricaoTextField.setText(turnoSelecionado.getDescricao());
        turnoTextField.setText(turnoSelecionado.getTurno());
        numAulasTextField.setText(String.valueOf(turnoSelecionado.getNumAulas()));
        intervaloTextField.setText(String.valueOf(turnoSelecionado.getIntervalo()));
    }
}
