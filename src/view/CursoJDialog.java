package view;

import adapter.CursosTableModel;
import static gradehoraria.GradeHoraria.cursoDAO;
import java.awt.Point;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import javax.swing.JTable;
import model.Curso;

/**
 *
 * @author Fernanda
 */
public class CursoJDialog extends javax.swing.JDialog {

    private final CursosTableModel cursoTableModel;
    private Curso cursoSelecionado;

    public CursoJDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        cursoTableModel = new CursosTableModel(cursoDAO.listar());
        initComponents();
        atualizarTabelaCursos();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        tabbedPane = new javax.swing.JTabbedPane();
        consultaPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        cursosTable = new javax.swing.JTable();
        cadastroPanel = new javax.swing.JPanel();
        codigoLabel = new javax.swing.JLabel();
        codigoTextField = new javax.swing.JTextField();
        nomeLabel = new javax.swing.JLabel();
        nomeTextField = new javax.swing.JTextField();
        nomeCurtoLabel = new javax.swing.JLabel();
        nomeCurtoTextField = new javax.swing.JTextField();
        idLabel = new javax.swing.JLabel();
        idTextField = new javax.swing.JTextField();
        salvarButton = new javax.swing.JButton();
        novoCursoButton = new javax.swing.JButton();
        editarCursoButton = new javax.swing.JButton();
        excluirCursoButton = new javax.swing.JButton();
        voltarButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Curso");

        tabbedPane.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabbedPaneMouseClicked(evt);
            }
        });

        cursosTable.setModel(cursoTableModel);
        cursosTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cursosTableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(cursosTable);

        javax.swing.GroupLayout consultaPanelLayout = new javax.swing.GroupLayout(consultaPanel);
        consultaPanel.setLayout(consultaPanelLayout);
        consultaPanelLayout.setHorizontalGroup(
            consultaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(consultaPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 415, Short.MAX_VALUE)
                .addContainerGap())
        );
        consultaPanelLayout.setVerticalGroup(
            consultaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(consultaPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabbedPane.addTab("Consulta", consultaPanel);

        codigoLabel.setText("Código:");

        nomeLabel.setText("Nome:");

        nomeCurtoLabel.setText("Nome curto:");

        idLabel.setText("ID:");

        idTextField.setEditable(false);

        salvarButton.setText("Salvar");
        salvarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salvarButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout cadastroPanelLayout = new javax.swing.GroupLayout(cadastroPanel);
        cadastroPanel.setLayout(cadastroPanelLayout);
        cadastroPanelLayout.setHorizontalGroup(
            cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cadastroPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(idLabel)
                    .addComponent(nomeLabel)
                    .addComponent(nomeCurtoLabel)
                    .addComponent(codigoLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(codigoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nomeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(idTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nomeCurtoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(salvarButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(159, Short.MAX_VALUE))
        );
        cadastroPanelLayout.setVerticalGroup(
            cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cadastroPanelLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(idLabel)
                    .addComponent(idTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(codigoLabel)
                    .addComponent(codigoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nomeLabel)
                    .addComponent(nomeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nomeCurtoLabel)
                    .addComponent(nomeCurtoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
                .addComponent(salvarButton, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        tabbedPane.addTab("Cadastro", cadastroPanel);

        novoCursoButton.setText("Novo");
        novoCursoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                novoCursoButtonActionPerformed(evt);
            }
        });

        editarCursoButton.setText("Editar");
        editarCursoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editarCursoButtonActionPerformed(evt);
            }
        });

        excluirCursoButton.setText("Excluir");
        excluirCursoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                excluirCursoButtonActionPerformed(evt);
            }
        });

        voltarButton.setText("Voltar");
        voltarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                voltarButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(tabbedPane, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(editarCursoButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(excluirCursoButton, javax.swing.GroupLayout.DEFAULT_SIZE, 73, Short.MAX_VALUE)
                    .addComponent(voltarButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(novoCursoButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(novoCursoButton, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(editarCursoButton, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(excluirCursoButton, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(voltarButton)
                .addContainerGap())
            .addComponent(tabbedPane)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void novoCursoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_novoCursoButtonActionPerformed
        abrirCadastro();
        cursoSelecionado = null;
    }//GEN-LAST:event_novoCursoButtonActionPerformed

    private void cursosTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cursosTableMouseClicked
        JTable tabela = (JTable) evt.getSource();
        Point pontoClique = evt.getPoint();
        int linhaTabela = tabela.rowAtPoint(pontoClique);
        if(linhaTabela != -1){
            Curso curso = cursoDAO.getCurso((int) tabela.getModel().getValueAt(linhaTabela, 0));
            if(evt.getClickCount() == 1){
                cursoSelecionado = curso;
            } if(evt.getClickCount() == 2){
                abrirCadastro();
                preencheCampos(curso);          
            }
        } 
    }//GEN-LAST:event_cursosTableMouseClicked

    private void excluirCursoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_excluirCursoButtonActionPerformed
        cursoDAO.remover(cursoSelecionado);
        atualizarTabelaCursos();
        limparCampos();
    }//GEN-LAST:event_excluirCursoButtonActionPerformed

    private void editarCursoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editarCursoButtonActionPerformed
        if(cursoSelecionado != null){
            abrirCadastro();
            preencheCampos(cursoSelecionado);
        } else {
           abrirConsulta();
           JOptionPane.showMessageDialog(null, "Selecione um curso antes.", "Falha ao remover", ERROR_MESSAGE);
        }
    }//GEN-LAST:event_editarCursoButtonActionPerformed

    private void salvarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salvarButtonActionPerformed
        if(camposPreenchidos()){
            Curso curso = getCursoCampos();
            if (cursoSelecionado == null) {
                cursoDAO.salvar(curso);
            } else {
                curso.setId(cursoSelecionado.getId());
                cursoDAO.atualizar(curso);
            }
            limparCampos();
            atualizarTabelaCursos();
            abrirConsulta();   
        } else {
            JOptionPane.showMessageDialog(null, "Preencha todos os campos.", "Falha ao remover", ERROR_MESSAGE);
        }
        
    }//GEN-LAST:event_salvarButtonActionPerformed

    private void tabbedPaneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabbedPaneMouseClicked
        limparCampos();
    }//GEN-LAST:event_tabbedPaneMouseClicked

    private void voltarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_voltarButtonActionPerformed
        dispose();
    }//GEN-LAST:event_voltarButtonActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                CursoJDialog dialog = new CursoJDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel cadastroPanel;
    private javax.swing.JLabel codigoLabel;
    private javax.swing.JTextField codigoTextField;
    private javax.swing.JPanel consultaPanel;
    private javax.swing.JTable cursosTable;
    private javax.swing.JButton editarCursoButton;
    private javax.swing.JButton excluirCursoButton;
    private javax.swing.JLabel idLabel;
    private javax.swing.JTextField idTextField;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel nomeCurtoLabel;
    private javax.swing.JTextField nomeCurtoTextField;
    private javax.swing.JLabel nomeLabel;
    private javax.swing.JTextField nomeTextField;
    private javax.swing.JButton novoCursoButton;
    private javax.swing.JButton salvarButton;
    private javax.swing.JTabbedPane tabbedPane;
    private javax.swing.JButton voltarButton;
    // End of variables declaration//GEN-END:variables

    private Curso getCursoCampos() {
        Curso curso = new Curso();
        
        curso.setNome(nomeTextField.getText());
        curso.setNomeCurto(nomeCurtoTextField.getText());
        curso.setCodigo(codigoTextField.getText());
        return curso;
    }

    private void limparCampos() {
        nomeCurtoTextField.setText("");
        nomeTextField.setText("");
        codigoTextField.setText("");
        idTextField.setText("");
    }

    private void atualizarTabelaCursos() {
        cursoTableModel.setCursos(cursoDAO.listar());
        getContentPane().repaint();
    }

    private void preencheCampos(Curso curso) {
        idTextField.setText(Integer.toString(curso.getId()));
        nomeCurtoTextField.setText(curso.getNomeCurto());
        nomeTextField.setText(curso.getNome());
        codigoTextField.setText(curso.getCodigo());
    }

    private void abrirCadastro() {
        tabbedPane.setSelectedIndex(1);
    }
    
    private void abrirConsulta(){
        tabbedPane.setSelectedIndex(0);
    }

    private boolean camposPreenchidos() {
        return !(nomeTextField.getText().isEmpty() ||
                codigoTextField.getText().isEmpty() ||
                nomeCurtoTextField.getText().isEmpty());
    }
}
