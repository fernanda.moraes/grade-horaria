package view;

import adapter.DisciplinasTableModel;
import static gradehoraria.GradeHoraria.disciplinaDAO;
import java.awt.Point;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import javax.swing.JTable;
import model.Disciplina;
import model.TipoSala;

/**
 *
 * @author Fernanda
 */
public class DisciplinaJDialog extends javax.swing.JDialog {

    private final DisciplinasTableModel disciplinasTableModel;
    private Disciplina disciplinaSelecionada;
    
    public DisciplinaJDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        disciplinasTableModel = new DisciplinasTableModel(disciplinaDAO.listar());
        initComponents();
        atualizarTabela();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        tabbedPane = new javax.swing.JTabbedPane();
        consultarPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        disciplinaTable = new javax.swing.JTable();
        addDisciplinaButton = new javax.swing.JButton();
        editarDisiciplinaButton = new javax.swing.JButton();
        excluirDisciplinaButton = new javax.swing.JButton();
        cadastroPanel = new javax.swing.JPanel();
        codigoLabel = new javax.swing.JLabel();
        nomeLabel = new javax.swing.JLabel();
        nomeTextField = new javax.swing.JTextField();
        chSemanalTextField = new javax.swing.JTextField();
        chSemanalLabel = new javax.swing.JLabel();
        chAulasGeminadasLabel = new javax.swing.JLabel();
        chAulasGeminadasTextField = new javax.swing.JTextField();
        codigoTextField = new javax.swing.JTextField();
        scrollPane1 = new java.awt.ScrollPane();
        jLabel1 = new javax.swing.JLabel();
        salvarButton = new javax.swing.JButton();
        limparButton = new javax.swing.JButton();
        voltarButton = new javax.swing.JButton();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        disciplinaTable.setModel(disciplinasTableModel);
        disciplinaTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                disciplinaTableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(disciplinaTable);

        addDisciplinaButton.setText("Adicionar");
        addDisciplinaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addDisciplinaButtonActionPerformed(evt);
            }
        });

        editarDisiciplinaButton.setText("Editar");
        editarDisiciplinaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editarDisiciplinaButtonActionPerformed(evt);
            }
        });

        excluirDisciplinaButton.setText("Excluir");
        excluirDisciplinaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                excluirDisciplinaButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout consultarPanelLayout = new javax.swing.GroupLayout(consultarPanel);
        consultarPanel.setLayout(consultarPanelLayout);
        consultarPanelLayout.setHorizontalGroup(
            consultarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(consultarPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(consultarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 473, Short.MAX_VALUE)
                    .addGroup(consultarPanelLayout.createSequentialGroup()
                        .addComponent(addDisciplinaButton, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(editarDisiciplinaButton, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(excluirDisciplinaButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        consultarPanelLayout.setVerticalGroup(
            consultarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(consultarPanelLayout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(consultarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addDisciplinaButton)
                    .addComponent(editarDisiciplinaButton)
                    .addComponent(excluirDisciplinaButton))
                .addGap(24, 24, 24))
        );

        tabbedPane.addTab("Consultar", consultarPanel);

        codigoLabel.setText("Código:");

        nomeLabel.setText("Nome:");

        chSemanalLabel.setText("Carga Horária Semanal:");

        chAulasGeminadasLabel.setText("Aulas Geminadas:");

        jLabel1.setText("Tipo de Sala:");

        salvarButton.setText("Salvar");
        salvarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salvarButtonActionPerformed(evt);
            }
        });

        limparButton.setText("Limpar");
        limparButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                limparButtonActionPerformed(evt);
            }
        });

        voltarButton.setText("Voltar");
        voltarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                voltarButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout cadastroPanelLayout = new javax.swing.GroupLayout(cadastroPanel);
        cadastroPanel.setLayout(cadastroPanelLayout);
        cadastroPanelLayout.setHorizontalGroup(
            cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cadastroPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(scrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(cadastroPanelLayout.createSequentialGroup()
                            .addComponent(salvarButton, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(limparButton)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(voltarButton))
                        .addGroup(cadastroPanelLayout.createSequentialGroup()
                            .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(cadastroPanelLayout.createSequentialGroup()
                                    .addGap(6, 6, 6)
                                    .addComponent(nomeLabel)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(nomeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, cadastroPanelLayout.createSequentialGroup()
                                    .addComponent(codigoLabel)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(codigoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGap(39, 39, 39)
                            .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(chAulasGeminadasLabel)
                                .addComponent(chSemanalLabel))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(chAulasGeminadasTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 82, Short.MAX_VALUE)
                                .addComponent(chSemanalTextField)))))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        cadastroPanelLayout.setVerticalGroup(
            cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cadastroPanelLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nomeLabel)
                    .addComponent(nomeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chAulasGeminadasLabel)
                    .addComponent(chAulasGeminadasTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(codigoLabel)
                    .addComponent(codigoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chSemanalLabel)
                    .addComponent(chSemanalTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(salvarButton)
                    .addComponent(limparButton)
                    .addComponent(voltarButton))
                .addContainerGap())
        );

        tabbedPane.addTab("Cadastro", cadastroPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabbedPane)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabbedPane)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addDisciplinaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addDisciplinaButtonActionPerformed
        abrirCadastro();
        disciplinaSelecionada = null;
    }//GEN-LAST:event_addDisciplinaButtonActionPerformed

    private void salvarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salvarButtonActionPerformed
        Disciplina disciplina;
        if(disciplinaSelecionada == null){
            disciplina = getDisiciplina();
            disciplinaDAO.salvar(disciplina);        
        } else {
            disciplinaDAO.atualizar(disciplinaSelecionada);
        }
        atualizarTabela();
        limparCampos();
        abrirConsulta();
    }//GEN-LAST:event_salvarButtonActionPerformed

    private void limparButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_limparButtonActionPerformed
        limparCampos();
    }//GEN-LAST:event_limparButtonActionPerformed

    private void voltarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_voltarButtonActionPerformed
        limparCampos();
        abrirConsulta();
    }//GEN-LAST:event_voltarButtonActionPerformed

    private void disciplinaTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_disciplinaTableMouseClicked
        Disciplina disciplina;
        JTable tabela = (JTable) evt.getSource();
        Point pontoClique = evt.getPoint();
        int linhaTabela = tabela.rowAtPoint(pontoClique);
        
        if(linhaTabela != -1){
            disciplina = disciplinaDAO.getDisicplina((int) tabela.getModel().getValueAt(linhaTabela, 0));
            if (evt.getClickCount() == 2){
                preencheCampos(disciplina);
            } else if (evt.getClickCount() == 1) {
                disciplinaSelecionada = disciplina;
            }
        }
    }//GEN-LAST:event_disciplinaTableMouseClicked

    private void excluirDisciplinaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_excluirDisciplinaButtonActionPerformed
        if(disciplinaSelecionada != null){
            abrirCadastro();
            preencheCampos(disciplinaSelecionada);
        } else {
           JOptionPane.showMessageDialog(null, "Selecione uma disiciplina antes.", "Falha ao remover", ERROR_MESSAGE);
        }

    }//GEN-LAST:event_excluirDisciplinaButtonActionPerformed

    private void editarDisiciplinaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editarDisiciplinaButtonActionPerformed
        if(disciplinaSelecionada != null){
            disciplinaDAO.remover(disciplinaSelecionada);
            atualizarTabela();
        } else {
           JOptionPane.showMessageDialog(null, "Selecione uma disiciplina antes.", "Falha ao Editar", ERROR_MESSAGE);
        }
    }//GEN-LAST:event_editarDisiciplinaButtonActionPerformed

    public static void main(String args[]) {
      
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DisciplinaJDialog dialog = new DisciplinaJDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addDisciplinaButton;
    private javax.swing.JPanel cadastroPanel;
    private javax.swing.JLabel chAulasGeminadasLabel;
    private javax.swing.JTextField chAulasGeminadasTextField;
    private javax.swing.JLabel chSemanalLabel;
    private javax.swing.JTextField chSemanalTextField;
    private javax.swing.JLabel codigoLabel;
    private javax.swing.JTextField codigoTextField;
    private javax.swing.JPanel consultarPanel;
    private javax.swing.JTable disciplinaTable;
    private javax.swing.JButton editarDisiciplinaButton;
    private javax.swing.JButton excluirDisciplinaButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JButton limparButton;
    private javax.swing.JLabel nomeLabel;
    private javax.swing.JTextField nomeTextField;
    private javax.swing.JButton salvarButton;
    private java.awt.ScrollPane scrollPane1;
    private javax.swing.JTabbedPane tabbedPane;
    private javax.swing.JButton voltarButton;
    // End of variables declaration//GEN-END:variables

    private void atualizarTabela() {
        disciplinasTableModel.setDisciplinas(disciplinaDAO.listar());
        getContentPane().repaint();
    }

    private Disciplina getDisiciplina() {
        Disciplina disciplina = new Disciplina();
        disciplina.setChAulasGeminadas(Integer.parseInt(chAulasGeminadasTextField.getText()));
        disciplina.setChSem(Integer.parseInt(chSemanalTextField.getText()));
        disciplina.setCodigo(codigoTextField.getText());
        disciplina.setNome(nomeTextField.getText());
        return disciplina;
    }

    private void limparCampos() {
        chAulasGeminadasTextField.setText("");
        chSemanalTextField.setText("");
        codigoTextField.setText("");
        nomeTextField.setText("");
    }

    private void abrirConsulta() {
        tabbedPane.setSelectedIndex(0); 
    }

    private void abrirCadastro() {
        tabbedPane.setSelectedIndex(1); 
    }

    private void preencheCampos(Disciplina disciplina) {
        abrirCadastro();
        chAulasGeminadasTextField.setText(String.valueOf(disciplina.getChAulasGeminadas()));
        chSemanalTextField.setText(String.valueOf(disciplina.getChSem()));
        codigoTextField.setText(disciplina.getCodigo());
        nomeTextField.setText(disciplina.getNome());
    }
}
