
package view;

import adapter.ProfessorComboBoxModel;
import static gradehoraria.GradeHoraria.disciplinaDAO;
import static gradehoraria.GradeHoraria.professorDAO;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import model.Disciplina;
import model.Professor;
import teste.CheckboxListItem;
import teste.CheckboxListRenderer;
import teste.DisciplinaItem;

/**
 *
 * @author Fernanda
 */
public class ProfessorDisciplinaJDialog extends javax.swing.JDialog implements ItemListener{
    
    private List<DisciplinaItem> disciplinasSelecionadas = new ArrayList<DisciplinaItem>();
    private ProfessorComboBoxModel professorComboBoxModel;
    private Professor professorSelecionado;
    
    public ProfessorDisciplinaJDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        professorComboBoxModel = new ProfessorComboBoxModel(professorDAO.listar());
        initComponents();

        JList<DisciplinaItem> list = configuraListaDisciplinas();

        list.addMouseListener(new MouseAdapter() {
           public void mouseClicked(MouseEvent event) {
              JList<DisciplinaItem> list = (JList<DisciplinaItem>) event.getSource();
              int index = list.locationToIndex(event.getPoint());
             
              DisciplinaItem item = (DisciplinaItem) list.getModel().getElementAt(index);
              item.setSelected(!item.isSelected());    
              alteraDisciplinasSelecionadas(item);
              
              list.repaint(list.getCellBounds(index, index));
           }
        });
        
        scrollPane1.add(list);
        
        professorComboBox.addItemListener(this);
   }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        professorLabel = new javax.swing.JLabel();
        professorComboBox = new javax.swing.JComboBox<>();
        chLabel = new javax.swing.JLabel();
        chValorLabel = new javax.swing.JLabel();
        chAlocadaLabel = new javax.swing.JLabel();
        chAlocadaValorLabel = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        scrollPane1 = new java.awt.ScrollPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        professorLabel.setText("Professor:");

        professorComboBox.setModel(professorComboBoxModel);

        chLabel.setText("Carga horária:");

        chValorLabel.setText("   ");

        chAlocadaLabel.setText("Carga horária alocada:");

        chAlocadaValorLabel.setText("   ");

        jButton1.setText("jButton1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(professorLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(professorComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(60, 60, 60)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(chLabel)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(chValorLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addComponent(chAlocadaLabel))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(chAlocadaValorLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 75, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(professorLabel)
                    .addComponent(professorComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chLabel)
                    .addComponent(chValorLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chAlocadaLabel)
                    .addComponent(chAlocadaValorLabel))
                .addGap(17, 17, 17)
                .addComponent(jButton1)
                .addGap(25, 25, 25)
                .addComponent(scrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

   
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ProfessorDisciplinaJDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ProfessorDisciplinaJDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ProfessorDisciplinaJDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ProfessorDisciplinaJDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ProfessorDisciplinaJDialog dialog = new ProfessorDisciplinaJDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel chAlocadaLabel;
    private javax.swing.JLabel chAlocadaValorLabel;
    private javax.swing.JLabel chLabel;
    private javax.swing.JLabel chValorLabel;
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox<Professor> professorComboBox;
    private javax.swing.JLabel professorLabel;
    private java.awt.ScrollPane scrollPane1;
    // End of variables declaration//GEN-END:variables

    private CheckboxListItem[] populaArray() {
        DisciplinaItem[]  itens = new DisciplinaItem[disciplinaDAO.listar().size()];
        HashMap<Integer, Disciplina> disciplinas = disciplinaDAO.listar();
        for (int i = 0; i < disciplinas.size(); i++) {
            itens[i] =  new DisciplinaItem(disciplinas.get(i), disciplinas.get(i).getNome());
        }
        
        return itens;
    }
    private void alteraDisciplinasSelecionadas(DisciplinaItem item) {
        if(item.isSelected()){
            if(!disciplinasSelecionadas.contains(item)){
                disciplinasSelecionadas.add(item);
            }
        } else {
            if(disciplinasSelecionadas.contains(item)){
                disciplinasSelecionadas.remove(item);
            }
        }
    }

    private JList configuraListaDisciplinas() {
        DisciplinaItem[]  disciplinas = (DisciplinaItem[]) populaArray();
        JList<DisciplinaItem> list = new JList<DisciplinaItem>(disciplinas);
        list.setCellRenderer(new CheckboxListRenderer());
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);    
        return list;
    }
    
    @Override
    public void itemStateChanged(ItemEvent e) {
        professorSelecionado = (Professor) e.getItem();
        alteraCargaHoraria();
    }

    private void alteraCargaHoraria() {
        chValorLabel.setText(String.valueOf(professorSelecionado.getCh()));
    }
    
}
