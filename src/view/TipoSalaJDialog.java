package view;

import adapter.TipoSalaTableModel;
import static gradehoraria.GradeHoraria.tipoSalaDAO;
import java.awt.Point;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import javax.swing.JTable;
import model.TipoSala;

/**
 *
 * @author Fernanda
 */
public class TipoSalaJDialog extends javax.swing.JDialog {
    
    private TipoSalaTableModel tipoSalaTableModel;
    private TipoSala tipoSalaSelecionado;
    
    public TipoSalaJDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        tipoSalaTableModel = new TipoSalaTableModel(tipoSalaDAO.listar());
        initComponents();
        atualizarTabela();
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabbedPane = new javax.swing.JTabbedPane();
        consultaPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tipoSalasTable = new javax.swing.JTable();
        cadastroPanel = new javax.swing.JPanel();
        tipoLabel = new javax.swing.JLabel();
        descricaoLabel = new javax.swing.JLabel();
        tipoTextField = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        descricaoTextArea = new javax.swing.JTextArea();
        padraoRadioButton = new javax.swing.JRadioButton();
        salvarButton = new javax.swing.JButton();
        opcoesPanel = new javax.swing.JPanel();
        novoButton = new javax.swing.JButton();
        editarButton = new javax.swing.JButton();
        excluirButton = new javax.swing.JButton();
        voltarButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        tabbedPane.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabbedPaneMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tabbedPaneMousePressed(evt);
            }
        });

        tipoSalasTable.setModel(tipoSalaTableModel);
        tipoSalasTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tipoSalasTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tipoSalasTable);

        javax.swing.GroupLayout consultaPanelLayout = new javax.swing.GroupLayout(consultaPanel);
        consultaPanel.setLayout(consultaPanelLayout);
        consultaPanelLayout.setHorizontalGroup(
            consultaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(consultaPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 418, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        consultaPanelLayout.setVerticalGroup(
            consultaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(consultaPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(84, Short.MAX_VALUE))
        );

        tabbedPane.addTab("Consulta", consultaPanel);

        tipoLabel.setText("Tipo:");

        descricaoLabel.setText("Descrição:");

        descricaoTextArea.setColumns(20);
        descricaoTextArea.setRows(5);
        jScrollPane2.setViewportView(descricaoTextArea);

        padraoRadioButton.setText("Padrão");

        salvarButton.setText("Salvar");
        salvarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salvarButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout cadastroPanelLayout = new javax.swing.GroupLayout(cadastroPanel);
        cadastroPanel.setLayout(cadastroPanelLayout);
        cadastroPanelLayout.setHorizontalGroup(
            cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cadastroPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(cadastroPanelLayout.createSequentialGroup()
                        .addGap(56, 56, 56)
                        .addComponent(padraoRadioButton))
                    .addGroup(cadastroPanelLayout.createSequentialGroup()
                        .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(descricaoLabel)
                            .addComponent(tipoLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tipoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(salvarButton, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(162, Short.MAX_VALUE))
        );
        cadastroPanelLayout.setVerticalGroup(
            cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cadastroPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tipoLabel)
                    .addComponent(tipoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(cadastroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(descricaoLabel)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addComponent(padraoRadioButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 74, Short.MAX_VALUE)
                .addComponent(salvarButton, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
        );

        tabbedPane.addTab("Cadastro", cadastroPanel);

        novoButton.setText("Novo");
        novoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                novoButtonActionPerformed(evt);
            }
        });

        editarButton.setText("Editar");
        editarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editarButtonActionPerformed(evt);
            }
        });

        excluirButton.setText("Excluir");
        excluirButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                excluirButtonActionPerformed(evt);
            }
        });

        voltarButton.setText("Voltar");

        javax.swing.GroupLayout opcoesPanelLayout = new javax.swing.GroupLayout(opcoesPanel);
        opcoesPanel.setLayout(opcoesPanelLayout);
        opcoesPanelLayout.setHorizontalGroup(
            opcoesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(excluirButton, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
            .addComponent(editarButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(novoButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(voltarButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        opcoesPanelLayout.setVerticalGroup(
            opcoesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(opcoesPanelLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(novoButton, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(editarButton, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(excluirButton, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(voltarButton)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(tabbedPane, javax.swing.GroupLayout.PREFERRED_SIZE, 448, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(opcoesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(opcoesPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(tabbedPane)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void editarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editarButtonActionPerformed
    }//GEN-LAST:event_editarButtonActionPerformed

    private void novoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_novoButtonActionPerformed
        abrirCadastro();
    }//GEN-LAST:event_novoButtonActionPerformed

    private void salvarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salvarButtonActionPerformed
        TipoSala tipoSala = getTipoSalaCampos();
        tipoSalaDAO.salvar(tipoSala);
        atualizarTabela();
        limparCampos();
        abrirConsulta();
    }//GEN-LAST:event_salvarButtonActionPerformed

    private void tipoSalasTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tipoSalasTableMouseClicked
        JTable tabela = (JTable) evt.getSource();
        Point pontoClique = evt.getPoint();
        int linhaTabela = tabela.rowAtPoint(pontoClique);
        if(linhaTabela != -1 && evt.getClickCount() == 2){
            TipoSala tipoSala = new TipoSala();
            tipoSala.setTipo((int) tabela.getModel().getValueAt(linhaTabela, 0));
            tipoSala.setDescricao((String) tabela.getModel().getValueAt(linhaTabela, 1));
            tipoSala.setPadrao((boolean) tabela.getModel().getValueAt(linhaTabela, 2));
             if (evt.getClickCount() == 2){
                preencheCampos(tipoSala);
            } else if (evt.getClickCount() == 1) {
                tipoSalaSelecionado = tipoSala;
            }
        }
    }//GEN-LAST:event_tipoSalasTableMouseClicked

    private void tabbedPaneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabbedPaneMouseClicked
    }//GEN-LAST:event_tabbedPaneMouseClicked

    private void tabbedPaneMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabbedPaneMousePressed
    }//GEN-LAST:event_tabbedPaneMousePressed

    private void excluirButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_excluirButtonActionPerformed
        if(tipoSalaSelecionado != null){
            tipoSalaDAO.remover(tipoSalaSelecionado);
            atualizarTabela();
        } else {
           JOptionPane.showMessageDialog(null, "Selecione um tipo de sala antes.", "Falha ao remover", ERROR_MESSAGE);
        }
    }//GEN-LAST:event_excluirButtonActionPerformed

    public static void main(String args[]) {
      
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                TipoSalaJDialog dialog = new TipoSalaJDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel cadastroPanel;
    private javax.swing.JPanel consultaPanel;
    private javax.swing.JLabel descricaoLabel;
    private javax.swing.JTextArea descricaoTextArea;
    private javax.swing.JButton editarButton;
    private javax.swing.JButton excluirButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton novoButton;
    private javax.swing.JPanel opcoesPanel;
    private javax.swing.JRadioButton padraoRadioButton;
    private javax.swing.JButton salvarButton;
    private javax.swing.JTabbedPane tabbedPane;
    private javax.swing.JLabel tipoLabel;
    private javax.swing.JTable tipoSalasTable;
    private javax.swing.JTextField tipoTextField;
    private javax.swing.JButton voltarButton;
    // End of variables declaration//GEN-END:variables

    private void abrirCadastro() {
        tabbedPane.setSelectedIndex(1); 
    }

    private TipoSala getTipoSalaCampos() {
        TipoSala tipoSala = new TipoSala();
        tipoSala.setDescricao(descricaoTextArea.getText());
        tipoSala.setPadrao(padraoRadioButton.isSelected());
        tipoSala.setTipo(Integer.parseInt(tipoTextField.getText()));    
        
        return tipoSala;
    }

    private void atualizarTabela() {
        tipoSalaTableModel.setTipoSalas(tipoSalaDAO.listar());
        getContentPane().repaint();
    }

    private void abrirConsulta() {
        tabbedPane.setSelectedIndex(0); 
    }

    private void limparCampos() {
        descricaoTextArea.setText("");
        padraoRadioButton.setSelected(false);
        tipoTextField.setText("");
    }

    private void preencheCampos(TipoSala tipoSala) {
        abrirCadastro();
        descricaoTextArea.setText(tipoSala.getDescricao());
        padraoRadioButton.setSelected(tipoSala.isPadrao());
        tipoTextField.setText(String.valueOf(tipoSala.getTipo()));
    }
}
