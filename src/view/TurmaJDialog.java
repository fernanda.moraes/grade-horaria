package view;

import adapter.CursoComboBox;
import adapter.TurmasTableModel;
import adapter.TurnoComboBox;
import gradehoraria.GradeHoraria;
import static gradehoraria.GradeHoraria.cursoDAO;
import static gradehoraria.GradeHoraria.turmaDAO;
import static gradehoraria.GradeHoraria.turnoDAO;
import model.Curso;
import model.Turma;
import model.Turno;

/**
 *
 * @author Fernanda
 */
public class TurmaJDialog extends javax.swing.JDialog {

    private TurmasTableModel turmasTableModel;
    private CursoComboBox cursoComboBoxModel;
    private TurnoComboBox turnoComboBoxModel;

    public TurmaJDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        cursoComboBoxModel = new CursoComboBox(cursoDAO.listar());
        turnoComboBoxModel = new TurnoComboBox(turnoDAO.listar());
        turmasTableModel = new TurmasTableModel(turmaDAO.listar());
        initComponents();
        atualizarTabelaTurma();
    }
  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable4 = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        numDiasLabel = new javax.swing.JLabel();
        cursoLabel = new javax.swing.JLabel();
        cursoComboBox = new javax.swing.JComboBox<>();
        numAulasTextField = new javax.swing.JTextField();
        addTurmaButton = new javax.swing.JButton();
        editarTurmaButton = new javax.swing.JButton();
        excluirButton = new javax.swing.JButton();
        descricaoTextField = new javax.swing.JTextField();
        identificacaoTextField = new javax.swing.JTextField();
        numAulasLabel = new javax.swing.JLabel();
        descricaoLabel = new javax.swing.JLabel();
        numDiasTextField = new javax.swing.JTextField();
        periodoLabel = new javax.swing.JLabel();
        turnoLabel = new javax.swing.JLabel();
        periodoTextField = new javax.swing.JTextField();
        identificacaoLabel = new javax.swing.JLabel();
        turnoComboBox = new javax.swing.JComboBox<>();
        jScrollPane5 = new javax.swing.JScrollPane();
        turmasTable = new javax.swing.JTable();
        codigoLabel = new javax.swing.JLabel();
        codigoTextField = new javax.swing.JTextField();

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable2);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(jTable3);

        jTable4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(jTable4);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Turma");

        numDiasLabel.setText("Número de dias:");

        cursoLabel.setText("Curso:");

        cursoComboBox.setModel(cursoComboBoxModel);
        cursoComboBox.setSelectedIndex(-1);

        addTurmaButton.setText("Adicionar ");
        addTurmaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addTurmaButtonActionPerformed(evt);
            }
        });

        editarTurmaButton.setText("Editar");

        excluirButton.setText("Excluir");
        excluirButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                excluirButtonActionPerformed(evt);
            }
        });

        numAulasLabel.setText("Número de aulas:");

        descricaoLabel.setText("Descrição:");

        periodoLabel.setText("Período:");

        turnoLabel.setText("Turno:");

        identificacaoLabel.setText("Identificação: ");

        turnoComboBox.setModel(turnoComboBoxModel);
        turnoComboBox.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                turnoComboBoxAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });

        turmasTable.setModel(turmasTableModel);
        jScrollPane5.setViewportView(turmasTable);

        codigoLabel.setText("Código:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(identificacaoLabel)
                            .addComponent(descricaoLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(descricaoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(identificacaoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(numAulasLabel)
                        .addGap(18, 18, 18)
                        .addComponent(numAulasTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(addTurmaButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(editarTurmaButton))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(numDiasLabel)
                                .addGap(18, 18, 18)
                                .addComponent(numDiasTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(27, 27, 27)
                        .addComponent(excluirButton)))
                .addGap(47, 47, 47)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(periodoLabel)
                    .addComponent(turnoLabel)
                    .addComponent(cursoLabel)
                    .addComponent(codigoLabel))
                .addGap(28, 28, 28)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cursoComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(turnoComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(codigoTextField, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(periodoTextField, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)))
                .addGap(0, 29, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(identificacaoLabel)
                    .addComponent(identificacaoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cursoComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cursoLabel))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(descricaoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(descricaoLabel)
                    .addComponent(turnoComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(turnoLabel))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(numAulasLabel)
                    .addComponent(numAulasTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(periodoLabel)
                    .addComponent(periodoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(numDiasLabel)
                    .addComponent(numDiasTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(codigoLabel)
                    .addComponent(codigoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addTurmaButton)
                    .addComponent(editarTurmaButton)
                    .addComponent(excluirButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 18, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void turnoComboBoxAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_turnoComboBoxAncestorAdded
    }//GEN-LAST:event_turnoComboBoxAncestorAdded

    private void addTurmaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addTurmaButtonActionPerformed
        Turma turma = getTurma();
        turmaDAO.salvar(turma);
        atualizarTabelaTurma();
        limparCampos();
    }//GEN-LAST:event_addTurmaButtonActionPerformed

    private void excluirButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_excluirButtonActionPerformed
    }//GEN-LAST:event_excluirButtonActionPerformed

  
    public static void main(String args[]) {
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                TurmaJDialog dialog = new TurmaJDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addTurmaButton;
    private javax.swing.JLabel codigoLabel;
    private javax.swing.JTextField codigoTextField;
    private javax.swing.JComboBox<Curso> cursoComboBox;
    private javax.swing.JLabel cursoLabel;
    private javax.swing.JLabel descricaoLabel;
    private javax.swing.JTextField descricaoTextField;
    private javax.swing.JButton editarTurmaButton;
    private javax.swing.JButton excluirButton;
    private javax.swing.JLabel identificacaoLabel;
    private javax.swing.JTextField identificacaoTextField;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTable jTable4;
    private javax.swing.JLabel numAulasLabel;
    private javax.swing.JTextField numAulasTextField;
    private javax.swing.JLabel numDiasLabel;
    private javax.swing.JTextField numDiasTextField;
    private javax.swing.JLabel periodoLabel;
    private javax.swing.JTextField periodoTextField;
    private javax.swing.JTable turmasTable;
    private javax.swing.JComboBox<Turno> turnoComboBox;
    private javax.swing.JLabel turnoLabel;
    // End of variables declaration//GEN-END:variables

    private Turma getTurma(){
        Turma turma = new Turma();
        turma.setCodigo(Integer.parseInt(codigoTextField.getText()));
        turma.setCurso((Curso)cursoComboBox.getSelectedItem());
        turma.setDescricao(descricaoTextField.getText());
        turma.setIdentificacao(identificacaoTextField.getText());
        turma.setNumAulas(Integer.parseInt(numAulasTextField.getText()));
        turma.setNumDias(Integer.parseInt(numDiasTextField.getText()));
        turma.setPeriodo(Integer.parseInt(periodoTextField.getText()));
        turma.setTurno((Turno)turnoComboBox.getSelectedItem());
        
        return turma;
    }

    private void atualizarTabelaTurma() {
        turmasTableModel.setTurmas(turmaDAO.listar());
        getContentPane().repaint();
    }

    private void limparCampos() {
        codigoTextField.setText("");
        cursoComboBox.setSelectedIndex(-1);
        turnoComboBox.setSelectedIndex(-1);
        descricaoTextField.setText("");
        identificacaoTextField.setText("");
        numAulasTextField.setText("");
        numDiasTextField.setText("");
        periodoTextField.setText("");
    }

}
