
package view;

import adapter.ProfessorTableModel;
import dao.ProfessorDAO;
import static gradehoraria.GradeHoraria.professorDAO;
import java.awt.Point;
import javax.swing.JTable;
import model.Professor;

/**
 *
 * @author Fernanda
 */
public class ProfessorJDialog extends javax.swing.JDialog {

    private ProfessorTableModel professorTableModel;
    
    public ProfessorJDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        professorTableModel = new ProfessorTableModel(professorDAO.listar());
        initComponents();
        atualizaTabela();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        ProfessorTable = new javax.swing.JTable();
        nomeLabel = new javax.swing.JLabel();
        emailLabel = new javax.swing.JLabel();
        chLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        nomeTextField = new javax.swing.JTextField();
        emailTextField = new javax.swing.JTextField();
        chTextField = new javax.swing.JTextField();
        apelidoTextField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        IDTextField = new javax.swing.JTextField();
        adicionarButton = new javax.swing.JButton();
        editarButton = new javax.swing.JButton();
        excluirButton = new javax.swing.JButton();

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        ProfessorTable.setModel(professorTableModel);
        ProfessorTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ProfessorTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(ProfessorTable);

        nomeLabel.setText("Nome:");

        emailLabel.setText("Email:");

        chLabel.setText("Carga Horária:");

        jLabel1.setText("Apelido:");

        jLabel2.setText("ID:");

        adicionarButton.setText("Adicionar");
        adicionarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adicionarButtonActionPerformed(evt);
            }
        });

        editarButton.setText("Editar");

        excluirButton.setText("Excluir");
        excluirButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                excluirButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(emailLabel)
                                    .addComponent(nomeLabel))
                                .addGap(12, 12, 12)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(IDTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(nomeTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 183, Short.MAX_VALUE)
                                            .addComponent(emailTextField))
                                        .addGap(43, 43, 43)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(chLabel, javax.swing.GroupLayout.Alignment.TRAILING))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(chTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(apelidoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(adicionarButton, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(editarButton, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(excluirButton)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(IDTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nomeLabel)
                    .addComponent(nomeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chLabel)
                    .addComponent(chTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(apelidoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(emailTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(emailLabel))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(editarButton)
                        .addComponent(excluirButton))
                    .addComponent(adicionarButton, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void adicionarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adicionarButtonActionPerformed
        Professor professor = getProfessorCampos();
        professorDAO.salvar(professor);
        limpaCampos();
        atualizaTabela();
    }//GEN-LAST:event_adicionarButtonActionPerformed

    private void ProfessorTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ProfessorTableMouseClicked
        JTable tabela = (JTable) evt.getSource();
        Point clique = evt.getPoint();
        int linhaClicada = tabela.rowAtPoint(clique);
        
        if(linhaClicada != -1 && evt.getClickCount() == 2){
            Professor professorSelecionado = getProfessorTabela(tabela, linhaClicada);
            preencheCampos(professorSelecionado);
        }
    }//GEN-LAST:event_ProfessorTableMouseClicked

    private void excluirButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_excluirButtonActionPerformed
        Professor professor = getProfessorCampos();
        professorDAO.remover(professor);
        atualizaTabela();
        limpaCampos();
    }//GEN-LAST:event_excluirButtonActionPerformed
    
    public static void main(String args[]) {
      
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ProfessorJDialog dialog = new ProfessorJDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField IDTextField;
    private javax.swing.JTable ProfessorTable;
    private javax.swing.JButton adicionarButton;
    private javax.swing.JTextField apelidoTextField;
    private javax.swing.JLabel chLabel;
    private javax.swing.JTextField chTextField;
    private javax.swing.JButton editarButton;
    private javax.swing.JLabel emailLabel;
    private javax.swing.JTextField emailTextField;
    private javax.swing.JButton excluirButton;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel nomeLabel;
    private javax.swing.JTextField nomeTextField;
    // End of variables declaration//GEN-END:variables

    private Professor getProfessorCampos() {
        Professor professor = new Professor();
        professor.setApelido(apelidoTextField.getText());
        professor.setCh(Integer.parseInt(chTextField.getText()));
        professor.setEmail(emailTextField.getText());
        professor.setNome(nomeTextField.getText());
        professor.setId(Integer.parseInt(IDTextField.getText()));
        return professor;
    }

    private void limpaCampos() {
        apelidoTextField.setText("");
        chTextField.setText("");
        emailTextField.setText("");
        nomeTextField.setText("");
        IDTextField.setText("");
    }

    private void atualizaTabela() {
        professorTableModel.setProfessores(professorDAO.listar());
        getContentPane().repaint();    }

    private Professor getProfessorTabela(JTable tabela, int linhaClicada) {
        int id = (int) tabela.getModel().getValueAt(linhaClicada, 0);
        Professor professor = professorDAO.getProfessor(id);

        return professor;
    }

    private void preencheCampos(Professor professorSelecionado) {
        apelidoTextField.setText(professorSelecionado.getApelido());
        chTextField.setText(String.valueOf(professorSelecionado.getCh()));
        emailTextField.setText(professorSelecionado.getEmail());
        nomeTextField.setText(professorSelecionado.getNome());
        IDTextField.setText(String.valueOf(professorSelecionado.getId()));
    }
}
