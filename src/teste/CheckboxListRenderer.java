/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teste;

import java.awt.Component;
import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author Fernanda
 */
public class CheckboxListRenderer extends JCheckBox implements
      ListCellRenderer<CheckboxListItem> {
 
   @Override
   public Component getListCellRendererComponent(
         JList<? extends CheckboxListItem> list, CheckboxListItem value,
         int index, boolean isSelected, boolean cellHasFocus) {
      setEnabled(list.isEnabled());
      setSelected(value.isSelected());
      setFont(list.getFont());
      setBackground(list.getBackground());
      setForeground(list.getForeground());
      setText(value.toString());
      return this;
   }
}