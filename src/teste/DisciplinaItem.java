
package teste;

import model.Disciplina;

/**
 *
 * @author Fernanda
 */
public class DisciplinaItem extends CheckboxListItem {
    
    private Disciplina disciplina;

    public DisciplinaItem(Disciplina disciplina, String label) {
        super(label);
        this.disciplina = disciplina;
    }
    
    public DisciplinaItem(String label) {
        super(label);
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }
    
}
