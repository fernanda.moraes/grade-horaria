
package dao;

import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Fernanda
 */
public interface DAO <Object> {
    
    HashMap<Integer, Object> listar();
    void salvar(Object t);
    void atualizar(Object t);
    void remover(Object t);
    
}
