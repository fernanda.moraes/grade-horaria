
package dao;

import java.util.HashMap;
import model.Professor;


/**
 *
 * @author Fernanda
 */
public class ProfessorDAO implements DAO<Professor>{

    private final HashMap<Integer, Professor> professores;
    private static int id;
    
    public ProfessorDAO() {
        this.professores = new HashMap<>();
        id = 0;
    }

    @Override
    public HashMap<Integer, Professor> listar() {
        return new HashMap<>(this.professores);
    }

    @Override
    public void salvar(Professor professor) {
        professor.setId(id);
        professores.put(id, professor);
        id++;
    }

    @Override
    public void atualizar(Professor professor) {
        professores.remove(professor.getId(), professor);
    }

    @Override
    public void remover(Professor professor) {
        professores.remove(professor.getId());
    }

    public Professor getProfessor(int id) {
        return professores.get(id);
    }
    
}
