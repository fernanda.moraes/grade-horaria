
package dao;

import java.util.HashMap;
import model.TipoSala;

/**
 *
 * @author Fernanda
 */
public class TipoSalaDAO implements DAO<TipoSala>{
    
    private final HashMap<Integer, TipoSala> tipoSalas;
    private static int id;
    
    public TipoSalaDAO() {
        id = 0;
        this.tipoSalas = new HashMap<>();
    }

    @Override
    public HashMap<Integer, TipoSala> listar() {
        return new HashMap<>(tipoSalas);
    }

    @Override
    public void salvar(TipoSala tipoSala) {
        tipoSala.setId(id);
        tipoSalas.put(id, tipoSala);
        id++;
    }

    @Override
    public void atualizar(TipoSala tipoSala) {
        tipoSalas.replace(tipoSala.getId(), tipoSala);
    }

    @Override
    public void remover(TipoSala tipoSala) {
       tipoSalas.remove(tipoSala.getId());
    }
    
    public TipoSala getTipoSala(int id){
        return tipoSalas.get(id);
    }
    
}
