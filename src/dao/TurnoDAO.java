package dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import model.Turno;

/**
 *
 * @author Fernanda
 */
public class TurnoDAO implements DAO<Turno>{

    private final HashMap<Integer, Turno> turnos;
    private static int id;
    
    public TurnoDAO() {
        this.turnos = new HashMap<> ();
        id = 0;
    }

    @Override
    public HashMap<Integer, Turno>  listar() {
        return new HashMap<> (turnos);
    }

    @Override
    public void salvar(Turno turno) {
        turno.setId(id);
        turnos.put(id, turno);
        id++;
    }

    @Override
    public void atualizar(Turno turno) {
        turnos.replace(turno.getId(), turno);
    }

    @Override
    public void remover(Turno turno) {
        turnos.remove(turno.getId());
    }
    
    public Turno getTurno(int id){
        return turnos.get(id);
    }
    
}
