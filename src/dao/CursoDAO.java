package dao;

import java.util.HashMap;
import model.Curso;

/**
 *
 * @author Fernanda
 */
public class CursoDAO implements DAO<Curso> {

    private final HashMap<Integer, Curso> cursos;
    private static int id;
    
    public CursoDAO() {
        this.cursos = new HashMap<>();
        CursoDAO.id = 0;
    }

    @Override
    public void salvar(Curso curso) {
        curso.setId(id);
        cursos.put(id, curso);
        id++;
    }

    @Override
    public HashMap<Integer, Curso> listar() {
        return new HashMap<>(cursos);
    }

    @Override
    public void atualizar(Curso curso) {
        cursos.replace(curso.getId(), curso);
  
    }

    @Override
    public void remover(Curso curso) {
        cursos.remove(curso.getId());
    }
    
    public Curso getCurso(int id){
        return cursos.get(id);
    }
}
