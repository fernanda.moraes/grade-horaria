
package dao;

import java.util.HashMap;
import model.Horario;

/**
 *
 * @author Fernanda
 */
public class HorarioDAO implements DAO<Horario>{

    private final HashMap<Integer, Horario> horarios;
    private static int id;
    
    public HorarioDAO() {
        horarios = new HashMap<>();
        id = 0;
    }

    @Override
    public HashMap<Integer, Horario> listar() {
        return new HashMap<>(horarios);
    }

    @Override
    public void salvar(Horario horario) {
        horario.setId(id);
        horarios.put(id, horario);
        id++;
    }

    @Override
    public void atualizar(Horario horario) {
        horarios.replace(horario.getId(), horario);
    }

    @Override
    public void remover(Horario horario) {
        horarios.remove(horario.getId());
    }
    
    public Horario getHorario(int id){
        return horarios.get(id);
    }
    
}
