package dao;

import java.util.HashMap;
import model.Turma;

/**
 *
 * @author Fernanda
 */
public class TurmaDAO implements DAO<Turma>{

    private final HashMap<Integer, Turma> turmas;
    private static int id;
        
    public TurmaDAO() {
        this.turmas = new HashMap<>();
        id = 0;
    }
    
    @Override
    public HashMap<Integer, Turma> listar() {
        return new HashMap<>(turmas);
    }

    @Override
    public void salvar(Turma turma) {
        turma.setId(id);
        turmas.put(id, turma);
        id++;
    }

    @Override
    public void atualizar(Turma turma) {
        turmas.replace(turma.getId(), turma);
    }

    @Override
    public void remover(Turma turma) {
        turmas.remove(turma.getId());
    }
    
    public Turma getTurma(int id){
        return turmas.get(id);
    }

}
