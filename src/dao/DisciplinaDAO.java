package dao;

import java.util.HashMap;
import model.Disciplina;

/**
 *
 * @author Fernanda
 */
public class DisciplinaDAO implements DAO<Disciplina>{
    
    private final HashMap<Integer, Disciplina> disciplinas;
    private static int id;
    
    public DisciplinaDAO() {
        this.disciplinas = new HashMap<>();
        id = 0;
    }
    
    @Override
    public HashMap<Integer, Disciplina> listar() {
        return new HashMap<> (disciplinas);
    }

    @Override
    public void salvar(Disciplina disciplina) {
        disciplina.setId(id);
        disciplinas.put(id, disciplina);
        id++;
    }

    @Override
    public void atualizar(Disciplina disciplina) {
        disciplinas.replace(disciplina.getId(), disciplina);
    }

    @Override
    public void remover(Disciplina disciplina) {
        disciplinas.remove(disciplina.getId());
    }
    
    public Disciplina getDisicplina(int id){
        return disciplinas.get(id);
    }
}
