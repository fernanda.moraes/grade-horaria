package gradehoraria;

import dao.CursoDAO;
import dao.DisciplinaDAO;
import dao.HorarioDAO;
import dao.ProfessorDAO;
import dao.TipoSalaDAO;
import dao.TurmaDAO;
import dao.TurnoDAO;
import javax.swing.JFrame;
import view.PrincipalJFrame;

/**
 *
 * @author Fernanda
 */
public class GradeHoraria {

    public static TurmaDAO turmaDAO;
    public static TurnoDAO turnoDAO;
    public static CursoDAO cursoDAO;
    public static DisciplinaDAO disciplinaDAO;
    public static TipoSalaDAO tipoSalaDAO;
    public static HorarioDAO horariosDAO;
    public static ProfessorDAO professorDAO;

    public static void main(String[] args) {
        
        turmaDAO = new TurmaDAO();
        turnoDAO = new TurnoDAO();
        cursoDAO = new CursoDAO();
        disciplinaDAO = new DisciplinaDAO();
        tipoSalaDAO = new TipoSalaDAO();
        professorDAO = new ProfessorDAO();
        horariosDAO = new HorarioDAO();
        
        JFrame frame = new PrincipalJFrame();
        frame.setVisible(true);
        
    }
    
}
